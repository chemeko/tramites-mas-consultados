import { Component, h, State } from '@stencil/core';

@Component({
  tag: 'tramites-mas-consultados-component',
  styleUrl: 'my-component.css',
  shadow: true,
})
export class MyComponent {

  @State() data: string;
  @State() dataTramites: Array<any>;;

  async componentWillLoad() {

    let urltitulo = 'https://api-interno.www.gov.co/api/ficha-tramites-y-servicios/secciones/los-mas-consultados-en-home';
    let urlTramites = 'https://api-interno.www.gov.co/api/ficha-tramites-y-servicios/LoMasConsultado/ObtenerLoMasConsultado';

    await fetch(urltitulo).then(response => {
      response.json().then(json => {
        this.data = json.data;
        console.log(this.data);
      });
    });

    await fetch(urlTramites).then(response => {
      response.json().then(json => {
        this.dataTramites = json.data;
        console.log(this.dataTramites);
      });
    });
  }

  validarImagen(urlImagen: string) {

    if (urlImagen == null || urlImagen == undefined || urlImagen == "")
      urlImagen = "https://www.cuballama.com/envios/images/imagen-no-encontrada.png";

    return urlImagen;
  }
  render() {
    return <div>
      <div id="tramites-mas-consultados" class="mt-5 mb-5">
        <div class="titulo-tramites" tabindex="0"
          aria-label="A continuación encontrará la sección Trámites mas consultados..">
          <h2>Trámites mas consultados.</h2>
        </div>
        <div class="d-flex align-items-center justify-content-between">
              <div id="tarjetas">
                <div class="row col-12 px-0 mx-0">
                  {this.dataTramites?.map(tramites =>
                    <div class="col-ms-12 col-md-6 col-lg-4">
                      <a role="link" class="tarjetas-link"
                        aria-label={tramites.nombre}
                        href="javascript:void(0)" >
                        <div class="tarjeta">
                          <img src={this.validarImagen(tramites.iconoCategoria)}
                            alt="" />
                          <span>{tramites.nombre}</span>
                        </div>
                      </a>
                    </div>
                  )}
                </div>
              </div>
        </div>
      </div>
    </div>
  }
}
