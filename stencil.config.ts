import { Config } from '@stencil/core';

export const config: Config = {
  bundles: [{ components: ['tramites-mas-consultados-component'] }],
  namespace: 'tramites-mas-consultados-stencil',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
  testing: {
    browserHeadless: 'new',
  },
};
